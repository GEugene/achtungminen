package com.example.AchtungMinen_1;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 17.12.13
 * Time: 22:02
 * To change this template use File | Settings | File Templates.
 */
public class MineFieldFactory {
    private int rowCnt;
    private int colCnt;
    private boolean[] mineMap = null;
    private boolean[] openCellMap = null;

    public MineFieldFactory(int rowCnt, int colCnt) {
        this.rowCnt = rowCnt;
        this.colCnt = colCnt;
    }

    public void setMineMap(boolean[] mineMap) {
        this.mineMap = mineMap;
    }

    public void setOpenCellMap(boolean[] openCellMap) {
        this.openCellMap = openCellMap;
    }

    public MineField create() throws Exception {
        Cell[] cells = new Cell[rowCnt*colCnt];
        int index = 0;
        Cell cell;
        MineField mineField;

        for(int row = 0; row < rowCnt; row++) {
            for(int col = 0; col < colCnt; col++) {
                cell = new Cell(index);
                cell.row = row;
                cell.col = col;
                setIsOpen(cell);
                setIsBomb(cell);
                cells[index] = cell;
                index++;
            }
        }
        mineField = new MineField(rowCnt, colCnt, cells);

        return mineField;
    }

    private void setIsOpen(Cell cell) {
        if (openCellMap != null) {
            cell.isOpen = openCellMap[cell.getIndex()];
        }
    }

    private void setIsBomb(Cell cell) {
        if (mineMap != null) {
            cell.isBomb = mineMap[cell.getIndex()];
        }
    }
}
