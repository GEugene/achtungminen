package com.example.AchtungMinen_1;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 08.12.13
 * Time: 21:41
 * To change this template use File | Settings | File Templates.
 */
public class MineField {
    private Cell[] cells;
    private int colCnt;
    private int rowCnt;

    /**
     *
     * @param rowCnt int
     * @param colCnt int
     * @param cells Cell[]
     * @throws Exception
     */
    public MineField(int rowCnt, int colCnt, Cell[] cells) throws Exception {
        this.cells = cells;
        this.colCnt = colCnt;
        this.rowCnt = rowCnt;

        if (colCnt < 1 || rowCnt < 1) {
            throw new Exception("Поле должно содержать хотя бы одну клетку");
        }

        refreshCounters();
    }

    public int getColCnt() {
        return colCnt;
    }

    public int getRowCnt() {
        return rowCnt;
    }

    /**
     *
     * @param index int
     * @return Cell
     */
    public Cell getCell(int index) {
        return cells[index];
    }

    /**
     *
     * @param row int
     * @param col int
     * @return Cell
     */
    public Cell getCell(int row, int col) {
        int index = row * colCnt + col;

        return cells[index];
    }

    /**
     *
     * @param cell int
     * @return Cell
     */
    public Cell[] getNeighborsFor(Cell cell) {
        int minRowIndex = 0;
        int maxRowIndex = rowCnt - 1;
        int minColIndex = 0;
        int maxColIndex = colCnt - 1;

        int startRow = Math.max(cell.row - 1, minRowIndex);
        int endRow = Math.min(cell.row + 1, maxRowIndex);
        int startCol = Math.max(cell.col - 1, minColIndex);
        int endCol = Math.min(cell.col + 1, maxColIndex);

        int size = (endRow - startRow + 1) * (endCol - startCol + 1) - 1;
        Cell[] neighbors = new Cell[size];

        int index = 0;
        for(int row = startRow; row <= endRow; row++) {
            for(int col = startCol; col <= endCol; col++) {
                if (cell.col == col && cell.row == row) {
                    continue;
                }

                neighbors[index] = getCell(row, col);
                ++index;
            }
        }

        return neighbors;
    }

    public void refreshCounters() {
        int index = 0;
        Cell[] neighbors;
        Cell cell;

        for(int row = 0; row < rowCnt; row++) {
            for(int col = 0; col < colCnt; col++) {
                cell = getCell(index);
                neighbors = getNeighborsFor(cell);
                cell.bombNearCnt = countBombs(neighbors);

                ++index;
            }
        }
    }

    private int countBombs(Cell[] cells) {
        int cnt = 0;
        for(int i = 0; i < cells.length; i++) {
            if (cells[i].isBomb) {
                ++cnt;
            }
        }

        return cnt;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        Cell cell;
        result.append("\n");

        for(int row = 0; row < rowCnt; row++) {
            for(int col = 0; col < colCnt; col++) {
                cell = getCell(row, col);
                result.append("[" + String.valueOf(row) + ", " + String.valueOf(col) + "]: " + String.valueOf(cell.isBomb));
                result.append(" " + String.valueOf(cell.bombNearCnt));
                result.append("\t");
            }
            result.append("\n");
        }

        return result.toString();
    }
}