package com.example.AchtungMinen_1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.HashSet;
import java.util.LinkedList;

public class GameField extends View implements View.OnTouchListener {
	private Paint p;
    private MineField mf;
    private int borderWidth = 2;
    private int cellSize = 40;
    private FieldDrawer drawer;

	/**
	 *
	 * @param context Context
	 */
	public GameField(Context context, MineField mf) {
		super(context);
        this.mf = mf;
		p = new Paint();
        drawer = new FieldDrawer(mf, getResources());

        setOnTouchListener(this);
        invalidate();
	}

    /**
	 *
	 * @param canvas Canvas
	 */
    @Override
	public void onDraw(Canvas canvas) {
        drawer.draw(canvas);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();

        Log.i("x", String.valueOf(x));
        Log.i("y", String.valueOf(y));

        try {
            Cell cell = getCellByCoords(x, y);
            handleTouchOnCell(cell);
            invalidate();
        } catch (Exception e) {

        }


        return true;
    }

    protected Cell getCellByCoords(float x, float y) {
        float col = x / (cellSize + borderWidth);
        float row = y / (cellSize + borderWidth);

        int cellCol = (int)Math.floor(col);
        int cellRow = (int)Math.floor(row);
        //Log.i("cell coord", String.valueOf(col) + " " + String.valueOf(row));

        return mf.getCell(cellRow, cellCol);
    }

    protected void handleTouchOnCell(Cell cell) {
        tryOpenCell(cell);
    }

    private void tryOpenCell(Cell cell) {
        cell.isOpen = true;

        if (!cell.isBomb && cell.bombNearCnt == 0) {
            tryOpenAreaNearCell(cell);
        }
    }

    private void tryOpenAreaNearCell(Cell cell) {
        LinkedList<Cell> queue = new LinkedList<Cell>();
        queue.add(cell);
        HashSet<Cell> visited = new HashSet<Cell>();

        while(queue.size() > 0) {
            Cell curCell = queue.remove();
            if (visited.contains(curCell)) {
                continue;
            }

            curCell.isOpen = true;
            visited.add(curCell);

            if (curCell.bombNearCnt == 0) {
                Cell[] neighbors = mf.getNeighborsFor(curCell);
                for(Cell c : neighbors) {
                    queue.add(c);
                }
            }
        }
    }
}