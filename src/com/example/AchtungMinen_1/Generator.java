package com.example.AchtungMinen_1;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 08.12.13
 * Time: 21:26
 * To change this template use File | Settings | File Templates.
 */
public class Generator {
    private int rowCnt;
    private int colCnt;
    private int mineCnt;
    private MineField mineField;

    /**
     *
     * @param rowCnt
     * @param colCnt
     * @param mineCnt
     */
    public Generator(int rowCnt, int colCnt, int mineCnt) throws Exception {
        this.rowCnt = rowCnt;
        this.colCnt = colCnt;
        this.mineCnt = mineCnt;

        if (mineCnt > rowCnt*colCnt) {
            throw new Exception("Количество мин превышает количество клеток.");
        }
    }

    public MineField generate() throws Exception {
        boolean[] mineMap = getMineMap();
        MineFieldFactory factory = new MineFieldFactory(rowCnt, colCnt);
        factory.setMineMap(mineMap);
        mineField = factory.create();

        return mineField;
    }

    private boolean[] getMineMap() {
        int size = rowCnt*colCnt;
        boolean[] mines = new boolean[size];

        // put mines
        for(int i = 0; i < size; i++) {
            boolean mine = false;
            if (i < mineCnt) {
                mine = true;
            }
            mines[i] = mine;
        }

        // shuffle
        Random rand = new Random();
        for(int i = 0; i < size; i++) {
            int j = rand.nextInt(size);
            boolean temp = mines[j];
            mines[j] = mines[i];
            mines[i] = temp;
        }

        return mines;
    }
}
