package com.example.AchtungMinen_1;

/**
 * Класс хранит данные ячейки игрового поля.
 */
public class Cell {
    int index;
    int row;
    int col;
    int bombNearCnt;
    boolean isBomb;
    boolean isOpen = false;

    public Cell(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
