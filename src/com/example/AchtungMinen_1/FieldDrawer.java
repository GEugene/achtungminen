package com.example.AchtungMinen_1;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created with IntelliJ IDEA.
 * User: Евгений
 * Date: 11.12.13
 * Time: 21:08
 * To change this template use File | Settings | File Templates.
 */
public class FieldDrawer {
    private MineField mf;
    private Paint p;

    private int left = 0;
    private int top = 0;
    private int right = 0;
    private int bottom = 0;
    private int borderWidth = 2;
    private int cellSize = 40;

    private Cell cell;
    private Resources res;
    private Canvas canvas;

    public FieldDrawer(MineField mf, Resources res) {
        this.mf = mf;
        this.res = res;
        p = new Paint();
    }

    public void draw(Canvas canvas) {
        int rowCnt = mf.getRowCnt();
        int colCnt = mf.getColCnt();
        this.canvas = canvas;

        for(int i = 0; i < rowCnt; i++) {
            for(int j = 0; j < colCnt; j++) {
                cell = mf.getCell(i, j);
                left = (j + 1)*borderWidth + j*cellSize;
                right = left + cellSize;
                top = (i + 1)*borderWidth + i*cellSize;
                bottom = top + cellSize;

                if (cell.isOpen) {
                    drawOpenCell();

                } else {
                    drawClosedCell();
                }
            }
        }
    }

    private void drawOpenCell() {
        p.setColor(res.getColor(R.color.cell_open));
        canvas.drawRect(left, top, right, bottom, p);

        if (cell.isBomb) {
            drawBomb();
        } else {
            drawBombFreeCell();
        }
    }

    private void drawClosedCell() {
        p.setColor(res.getColor(R.color.cell_closed));
        canvas.drawRect(left, top, right, bottom, p);
    }

    private void drawBomb() {
        p.setTextSize(30);
        p.setColor(res.getColor(R.color.bomb_counter));
        String text = "x";
        canvas.drawText(text, left + 2, bottom - 5, p);
    }

    private void drawBombFreeCell() {
        if (cell.bombNearCnt > 0) {
            p.setTextSize(30);
            p.setColor(res.getColor(R.color.bomb_counter));
            String text = String.valueOf(cell.bombNearCnt);
            canvas.drawText(text, left + 2, bottom - 5, p);
        }
    }
}
