package com.example.AchtungMinen_1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class AchtungMinenActivity extends Activity {
    private MineField mf;

    private int rowCount = 10;
    private int colCount = 10;
    private int mineCount = 7;

    static final String STATE_ROW_CNT = "rowCount";
    static final String STATE_COL_CNT = "colCount";
    static final String STATE_BOMB_LOCATION = "bombLocationList";
    static final String STATE_CELL_OPEN = "cellOpenList";

    private Bundle prevState = null;
    private View fieldView = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prevState = savedInstanceState;

        if (prevState != null && prevState.getInt(STATE_ROW_CNT, 0) > 0) {
            try {
                restoreMineField(prevState);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            showMineFieldView();

        } else {
            showMainMenuView();
        }

        Log.i("dump", "onCreate");
    }

    protected void showMainMenuView() {
        setContentView(R.layout.main);
    }

    public void startNewGame(View view) {
        try {
            createNewMineField();
            showMineFieldView();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMineFieldView() {
        if (fieldView == null) {
            fieldView = new GameField(this, mf);
        }
        setContentView(fieldView);
    }

    protected void createNewMineField() throws Exception {
        Generator gen = new Generator(rowCount, colCount, mineCount);
        mf = gen.generate();
    }

    protected void restoreMineField(Bundle savedInstanceState) throws Exception {
        boolean[] bombMap = savedInstanceState.getBooleanArray(STATE_BOMB_LOCATION);
        boolean[] openCellMap = savedInstanceState.getBooleanArray(STATE_CELL_OPEN);

        int restoredRowCnt = savedInstanceState.getInt(STATE_ROW_CNT);
        int restoredColCnt = savedInstanceState.getInt(STATE_COL_CNT);
        MineFieldFactory factory = new MineFieldFactory(restoredRowCnt, restoredColCnt);
        factory.setMineMap(bombMap);
        factory.setOpenCellMap(openCellMap);
        mf = factory.create();

        int size = openCellMap.length;
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < size; i++) {
            sb.append(String.valueOf(openCellMap[i]));
            sb.append(", ");
        }
        Log.i("dump", sb.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i("dump", "onSaveInstanceState");

        if (mf != null) {

            outState.putInt(STATE_ROW_CNT, rowCount);
            outState.putInt(STATE_COL_CNT, colCount);

            int size = rowCount*colCount;
            boolean[] bombMap = new boolean[size];
            boolean[] openCellMap = new boolean[size];
            Cell cell;

            for(int i = 0; i < size; i++) {
                cell = mf.getCell(i);
                bombMap[i] = cell.isBomb;
                openCellMap[i] = cell.isOpen;
            }

            outState.putBooleanArray(STATE_BOMB_LOCATION, bombMap);
            outState.putBooleanArray(STATE_CELL_OPEN, openCellMap);
        }


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i("dump", "onRestoreInstanceState");
        int restoredRowCnt = savedInstanceState.getInt(STATE_ROW_CNT, 0);
        int restoredColCnt = savedInstanceState.getInt(STATE_COL_CNT, 0);
        Log.i("dump", "rowCnt: " + String.valueOf(restoredRowCnt));
        Log.i("dump", "colCnt: " + String.valueOf(restoredColCnt));
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.i("dump", "onStop");
        /*FileOutputStream outputStream;
        DateFormat df = DateFormat.getTimeInstance();
        String time = df.format(new Date(0));

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(time.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.i("dump", "onDestroy");
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        Log.i("dump", "onRestart");
    }
}
